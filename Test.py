import os

YES = True
NO = not(YES)
LEN_LEFT_LIMIT = 0
LEN_RIGHT_LIMIT = 4 # open interval (LEN_LEFT_LIMIT, LEN_RIGHT_LIMIT)
POSITIVE_LIMIT = 25 # up to and including
THEMES_QUANTITY = POSITIVE_LIMIT
THEME_STEP = 25
answ_file_name = "answ.txt"
res_file_name = "res.txt"
directory_path = os.path.dirname(os.path.abspath(__file__))
result = dict(Hyp = 0, C = 0, L = 0, A = 0, S = 0, P = 0, Sch = 0, Epi = 0, Hys = 0, Un = 0, Co = 0, O = 0, D = 0, T = 0, B = 0, Ema = 0, d = 0, M = 0, F = 0, Alc = 0, Mist = 0)
result_extra = {key : val for key, val in result.items() if key != "Mist"}
answer_choices = [ [] for x in range(0, POSITIVE_LIMIT)]
answer_choices_left_limit = 0
answer_choices_right_limit = [0 for x in range(0, THEMES_QUANTITY)]

answer_choices_right_limit[0] = 12
answer_choices_right_limit[1] = 12
answer_choices_right_limit[2] = 14
answer_choices_right_limit[3] = 13
answer_choices_right_limit[4] = 19
answer_choices_right_limit[5] = 13
answer_choices_right_limit[6] = 14
answer_choices_right_limit[7] = 12
answer_choices_right_limit[8] = 13
answer_choices_right_limit[9] = 13
answer_choices_right_limit[10] = 15
answer_choices_right_limit[11] = 17
answer_choices_right_limit[12] = 12
answer_choices_right_limit[13] = 11
answer_choices_right_limit[14] = 13
answer_choices_right_limit[15] = 10
answer_choices_right_limit[16] = 13
answer_choices_right_limit[17] = 10
answer_choices_right_limit[18] = 12
answer_choices_right_limit[19] = 13
answer_choices_right_limit[20] = 14
answer_choices_right_limit[21] = 12
answer_choices_right_limit[22] = 15
answer_choices_right_limit[23] = 13
answer_choices_right_limit[24] = 13

possible_answers = [str(n) for n in range(0, max(answer_choices_right_limit) + 1)]

class DuplicationError(Exception):
    """Exception signals that there are more than 1 similar answer choices in one line"""
    def __init__(self, n):
        self.n = n

class LengthError(Exception):
    """Exception signals that the line length is out of the bounds specified by LEN_LEFT_LIMIT and LEN_RIGHT_LIMIT"""
    def __init__(self, n):
        self.n = n

class ZeroNotAloneError(Exception):
    """Exception signals that 0 answer choice is not the only choice in one line"""
    def __init__(self, n):
        self.n = n

class OutOfBoundsError(Exception):
    """Exception signals that answ answer choice in line n does not belong to the interval [answer_choices_left_limit[n], answer_choices_right_limit[n] ]"""
    def __init__(self, n, answ):
        self.n = n
        self.answ = answ

class MistypeError(Exception):
    """Exception signals that answ answer choice in line n is not a proper answer choice, which is specified by possible_answers"""
    def __init__(self, n, answ):
        self.n = n
        self.answ = answ

class UndefinedGenderError(Exception):
    "Exception signals that the specified gender is neither M nor F"
    pass

def epi_mistakes(line, n):
    """Checks how many self-excluding choices were chosen comparing most appropriate and most unappropriate choices from the same theme"""

    if n < POSITIVE_LIMIT:
        answer_choices[n - 1] = line.copy()
    elif set(line + answer_choices[n - 1 - THEME_STEP]) != {0}:
        result["Mist"] += len(line + answer_choices[n - 1 - THEME_STEP]) - len(set(line + answer_choices[n - 1 - THEME_STEP]))

def process_res(line, n):
    """Input: List, Number. Output:None
        Increments(sometimes decrements in case of 'Alc') parameters in results according to the answer checklist."""

    inlen = len(line)
    zero = NO

    epi_mistakes(line, n)

# Most appropriate statements part

    while line != [] and n == 1:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["A"] += 1
        elif ans == 2:
            result["Hyp"] += 1
            result["M"] += 2
        elif ans == 3:
            result["C"] += 1
        elif ans == 4:
            result["P"] += 1
        elif ans == 5:
            result["A"] += 2
        elif ans == 8:
            result["C"] += 1
        elif ans == 9:
            result["Un"] += 2
            result["D"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)


    while line != [] and n == 2:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
            result["Un"] += 1
        elif ans == 2:
            result["P"] += 1
        elif ans == 6:
            result["C"] += 1
            result["A"] += 1
        elif ans == 7:
            result["Sch"] += 1
        elif ans == 10:
            result["S"] += 1
            result["L"] += 1
        elif ans == 11:
            result["T"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 3:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
            result["Un"] += 1
            result["M"] += 2
        elif ans == 3:
            result["Co"] += 1
        elif ans == 4:
            result["d"] += 1
            result["A"] += 1
        elif ans == 6:
            result["C"] += 1
        elif ans == 7:
            result["A"] += 1
        elif ans == 10:
            result["A"] += 1
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 4:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Epi"] += 1
        elif ans == 3:
            result["T"] += 1
        elif ans == 4:
            result["C"] += 1
        elif ans == 6:
            result["F"] += 1
        elif ans == 8:
            result["A"] += 1
        elif ans == 9:
            result["Un"] += 2
        elif ans == 11:
            result["d"] += 1
        elif ans == 12:
            result["C"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 5:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Sch"] += 1
        elif ans == 2:
            result["T"] += 1
        elif ans == 3:
            result["A"] += 1
        elif ans == 5:
            result["L"] += 2
            result["P"] += 1
        elif ans == 6:
            result["S"] += 1
            result["T"] += 1
        elif ans == 7:
            result["M"] += 1
        elif ans == 11:
            result["Un"] += 1
        elif ans == 15:
            result["M"] += 1
        elif ans == 17:
            result["C"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 6:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Alc"] += 2
        elif ans == 3:
            result["Alc"] += 1
            result["M"] += 1
        elif ans == 4:
            result["Hyp"] += 2
            result["Alc"] += 2
        elif ans == 5:
            result["Alc"] -= 1
        elif ans == 6:
            result["Alc"] -= 1
        elif ans == 7:
            result["S"] += 1
            result["Alc"] -= 3
        elif ans == 8:
            result["Alc"] += 1
        elif ans == 9:
            result["P"] += 1
        elif ans == 10:
            result["Alc"] -= 3
            result["S"] += 1
            result["P"] += 1
            result["Sch"] += 1
        elif ans == 12:
            result["C"] += 1
            result["d"] += 1
        elif ans == 13:
            result["A"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 7:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["D"] += 1
            result["Un"] += 2
        elif ans == 10:
            result["M"] += 1
        elif ans == 14:
            result["L"] += 1
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 8:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 4:
            result["Hys"] += 2
        elif ans == 5:
            result["A"] += 1
            result["S"] += 1
        elif ans == 6:
            result["Co"] += 1
            result["F"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 9:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["C"] += 1
        elif ans == 4:
            result["Hys"] += 1
            result["M"] += 1
        elif ans == 6:
            result["L"] += 1
            result["A"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 10:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["P"] += 1
        elif ans == 3:
            result["L"] += 2
            result["B"] += 1
        elif ans == 5:
            result["Ema"] += 1
        elif ans == 8:
            result["Ema"] += 1
            result["F"] += 2
        elif ans == 9:
            result["P"] += 2
        elif ans == 10:
            result["M"] += 1
        elif ans == 11:
            result["Ema"] += 1
        elif ans == 12:
            result["Ema"] += 1
            result["Sch"] += 1
        elif ans == 13:
            result["Epi"] += 1
            result["d"] += 1
        elif ans == 0:
            result["O"] += 1
            result["Ema"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 11:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["D"] += 1
        elif ans == 3:
            result["C"] += 1
            result["Hyp"] += 1
        elif ans == 4:
            result["Co"] += 1
        elif ans == 7:
            result["S"] += 2
        elif ans == 14:
            result["L"] += 1
        elif ans == 15:
            result["A"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 12:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 5:
            result["M"] += 2
        elif ans == 7:
            result["Hyp"] += 1
        elif ans == 10:
            result["B"] += 1
        elif ans == 16:
            result["d"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 13:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Epi"] += 2
        elif ans == 8:
            result["C"] += 1
        elif ans == 9:
            result["C"] += 1
            result["Hyp"] += 1
        elif ans == 12:
            result["S"] += 2
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 14:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hys"] += 1
        elif ans == 2:
            result["Sch"] += 2
            result["S"] += 1
        elif ans == 3:
            result["Hyp"] += 1
            result["Epi"] += 1
            result["Un"] += 1
        elif ans == 4:
            result["L"] += 1
            result["A"] += 1
        elif ans == 6:
            result["d"] += 1
        elif ans == 8:
            result["Sch"] += 1
        elif ans == 11:
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 15:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 3:
            result["M"] += 1
        elif ans == 5:
            result["Hyp"] += 2
        elif ans == 7:
            result["P"] += 1
        elif ans == 8:
            result["Epi"] += 1
        elif ans == 10:
            result["P"] += 1
        elif ans == 11:
            result["d"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 16:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 5:
            result["Sch"] += 1
        elif ans == 8:
            result["d"] += 1
        elif ans == 9:
            result["L"] += 1
            result["Epi"] += 1
        elif ans == 10:
            result["P"] += 1
            result["Epi"] += 1
            result["Sch"] += 2
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 17:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Sch"] += 1
            result["B"] += 1
        elif ans == 3:
            result["Epi"] += 1
        elif ans == 5:
            result["Hys"] += 1
        elif ans == 7:
            result["Epi"] += 2
            result["d"] += 1
        elif ans == 10:
            result["P"] += 1
        elif ans == 13:
            result["Co"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 18:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
        elif ans == 2:
            result["S"] += 1
        elif ans == 3:
            result["S"] += 1
            result["M"] += 1
        elif ans == 6:
            result["B"] += 1
        elif ans == 7:
            result["F"] += 1
        elif ans == 9:
            result["Hyp"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 19:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["C"] += 1
        elif ans == 7:
            result["P"] += 2
        elif ans == 8:
            result["S"] += 1
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 20:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Sch"] += 2
            result["Ema"] += 1
        elif ans == 5:
            result["Ema"] += 1
        elif ans == 7:
            result["Un"] += 1
            result["Ema"] += 1
            result["d"] += 1
        elif ans == 8:
            result["T"] += 1
        elif ans == 9:
            result["A"] += 1
        elif ans == 10:
            result["Ema"] += 1
        elif ans == 11:
            result["C"] += 1
        elif ans == 12:
            result["Ema"] += 1
        elif ans == 0:
            result["O"] += 1
            result["Ema"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 21:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["C"] += 1
            result["P"] += 1
            result["M"] += 1
        elif ans == 2:
            result["Epi"] += 2
        elif ans == 3:
            result["D"] += 1
            result["Ema"] += 1
        elif ans == 4:
            result["Ema"] += 1
        elif ans == 6:
            result["C"] += 1
            result["L"] += 1
        elif ans == 8:
            result["Ema"] += 1
        elif ans == 11:
            result["Ema"] += 1
            result["Hys"] += 2
            result["F"] += 2
        elif ans == 13:
            result["Ema"] += 1
            result["F"] += 3
        elif ans == 14:
            result["Ema"] += 1
            result["F"] += 2
        elif ans == 0:
            result["O"] += 1
            result["Ema"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 22:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Ema"] += 1
        elif ans == 2:
            result["Hyp"] += 2
            result["Ema"] += 1
        elif ans == 4:
            result["d"] += 1
        elif ans == 5:
            result["Ema"] += 1
        elif ans == 7:
            result["Ema"] += 1
        elif ans == 10:
            result["d"] += 1
        elif ans == 11:
            result["F"] += 1
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            result["Ema"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 23:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["Hyp"] += 1
        elif ans == 4:
            result["D"] += 1
        elif ans == 7:
            result["Hyp"] += 1
        elif ans == 8:
            result["S"] += 2
        elif ans == 10:
            result["L"] += 1
        elif ans == 13:
            result["Hys"] += 1
        elif ans == 14:
            result["Epi"] += 2
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 24:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
            result["Un"] += 2
            result["Hys"] += 1
            result["Epi"] += 1
        elif ans == 2:
            result["Epi"] += 1
        elif ans == 3:
            result["C"] += 1
        elif ans == 4:
            result["F"] += 1
        elif ans == 6:
            result["d"] += 1
        elif ans == 13:
            result["C"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 25:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["M"] += 1
        elif ans == 2:
            result["T"] += 2
        elif ans == 3:
            result["A"] += 1
        elif ans == 8:
            result["Hys"] += 1
            result["Epi"] += 1
        elif ans == 11:
            result["d"] += 1
        elif ans == 12:
            result["Sch"] += 1
            result["Hys"] += 1
        elif ans == 13:
            result["S"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1]:
            raise OutOfBoundsError(n, ans)

# Most INappropriate statements part.

    while line != [] and n == 26:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["d"] += 1
        elif ans == 10:
            result["S"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)


    while line != [] and n == 27:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["Hyp"] += 1
        elif ans == 10:
            result["d"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 28:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["Epi"] += 1
        elif ans == 4:
            result["B"] += 1
        elif ans == 5:
            result["C"] += 1
        elif ans == 14:
            result["A"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 29:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["L"] += 1
        elif ans == 2:
            result["Co"] += 1
        elif ans == 7:
            result["L"] += 1
            result["M"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 30:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 3:
            result["C"] += 1
            result["Epi"] += 1
            result["M"] += 1
        elif ans == 16:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 31:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["C"] += 1
        elif ans == 4:
            result["S"] += 1
            result["Alc"] -= 1
        elif ans == 5:
            result["Alc"] += 2
        elif ans == 6:
            result["Alc"] += 1
        elif ans == 7:
            result["C"] += 1
            result["L"] += 1
            result["Alc"] += 2
        elif ans == 8:
            result["F"] += 1
        elif ans == 10:
            result["Alc"] += 1
        elif ans == 11:
            result["Hys"] += 1
        elif ans == 13:
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            result["Epi"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 32:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["M"] += 1
        elif ans == 2:
            result["C"] += 1
        elif ans == 5:
            result["Un"] += 2
        elif ans == 7:
            result["B"] += 1
            result["Co"] += 1
        elif ans == 8:
            result["Hys"] += 2
        elif ans == 11:
            result["L"] += 1
            result["S"] += 1
        elif ans == 13:
            result["L"] += 1
            result["Hyp"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 33:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["A"] += 1
            result["B"] += 1
        elif ans == 4:
            result["Co"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 34:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["M"] += 1
        elif ans == 2:
            result["Epi"] += 1
            result["Hys"] += 1
            result["D"] += 1
        elif ans == 6:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 35:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 5:
            result["Co"] += 1
        elif ans == 11:
            result["Hyp"] += 1
            result["L"] += 1
            result["P"] += 1
            result["Epi"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 36:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 3:
            result["d"] += 1
        elif ans == 6:
            result["Hys"] += 1
        elif ans == 12:
            result["D"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 37:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["Epi"] += 1
            result["F"] += 1
        elif ans == 7:
            result["S"] += 1
        elif ans == 10:
            result["S"] += 1
        elif ans == 13:
            result["P"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 38:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
        elif ans == 2:
            result["Epi"] += 2
        elif ans == 8:
            result["Hyp"] += 1
        elif ans == 9:
            result["S"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 39:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Sch"] += 2
        elif ans == 3:
            result["S"] += 1
            result["Sch"] += 3
            result["P"] += 1
        elif ans == 5:
            result["Epi"] += 1
            result["Hys"] += 1
            result["B"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 40:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 41:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 6:
            result["Epi"] += 1
        elif ans == 7:
            result["Hys"] += 1
            result["D"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 42:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["P"] += 2
            result["Epi"] += 1
        elif ans == 2:
            result["Epi"] += 2
        elif ans == 3:
            result["B"] += 1
        elif ans == 8:
            result["L"] += 1
        elif ans == 10:
            result["C"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 43:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["S"] += 2
        elif ans == 5:
            result["Epi"] += 2
        elif ans == 10:
            result["Un"] += 1
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)


    while line != [] and n == 44:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 4:
            result["Hys"] += 1
        elif ans == 9:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 45:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Co"] += 1
        elif ans == 5:
            result["L"] += 1
        elif ans == 10:
            result["P"] += 2
        elif ans == 11:
            result["d"] += 1
        elif ans == 12:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            result["A"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 46:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 9:
            result["Hys"] += 1
        elif ans == 12:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 47:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Co"] += 1
        elif ans == 2:
            result["S"] += 1
        elif ans == 4:
            result["Hyp"] += 1
        elif ans == 5:
            result["L"] += 1
        elif ans == 7:
            result["C"] += 1
        elif ans == 8:
            result["d"] += 3
        elif ans == 9:
            result["Hys"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 48:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 4:
            result["C"] += 1
        elif ans == 7:
            result["S"] += 1
        elif ans == 11:
            result["Hys"] += 2
        elif ans == 12:
            result["L"] += 1
        elif ans == 14:
            result["L"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 49:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 2:
            result["Epi"] += 2
            result["Un"] += 1
        elif ans == 9:
            result["Hys"] += 1
        elif ans == 11:
            result["Sch"] += 2
            result["d"] += 2
        elif ans == 13:
            result["d"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    while line != [] and n == 50:

        ans = line.pop(0)
        if not ans in possible_answers:
            raise MistypeError(n, ans)
        else:
            ans = int(ans)

        if ans == 1:
            result["Hyp"] += 1
        elif ans == 6:
            result["P"] += 2
        elif ans == 8:
            result["A"] += 1
        elif ans == 10:
            result["M"] += 1
        elif ans == 13:
            result["M"] += 1
        elif ans == 0:
            result["O"] += 1
            zero = YES
        elif ans < answer_choices_left_limit or ans > answer_choices_right_limit[n - 1 - THEME_STEP]:
            raise OutOfBoundsError(n, ans)

    if zero == YES and inlen > 1:
        raise ZeroNotAloneError(n)

def length_checker(line, n):
    """List Number -> List; Checks if the given list is not too long or too short. If it does, raises an exception, remembering the line number, where it has occured; otherwise returns the given list"""

    if  len(line) <= LEN_LEFT_LIMIT or len(line) >= LEN_RIGHT_LIMIT:
        raise LengthError(n)

    return line, n

def duplicates_checker(line, n):
    """List Number -> List; Checks if the given list has duplicates. If it does, raises an exception, remembering the line number, where it has occured; otherwise returns the given list"""

    if len(line) != len(set(line)):
        raise DuplicationError(n)

    return line, n

def extra_points():
    """Ads extra points based on the initial result"""

    if 0 <= result["Hyp"] <= 1:
        result_extra["P"] += 1
        result_extra["S"] += 1

    if result["C"] >= 6:
        result_extra["L"] += 1

    if result["A"] >= 4:
        result_extra["L"] += 1

    if 0 <= result["P"] <= 1:
        result_extra["Un"] += 1

    if 0 <= result["Un"] <= 1:
        result_extra["P"] += 1

    if result["Co"] == 0:
        result_extra["Sch"] += 2
        result_extra["Hys"] += 1

    if result["Co"] == 1:
        result_extra["Sch"] += 1

    if result["D"] >= 6:
        result_extra["Un"] += 1

    if result["T"] > result["D"]:
        result_extra["P"] += 2
        result_extra["C"] += 1

    if result["B"] == 5:
        result_extra["Epi"] += 1

    if result["B"] >= 6:
        result_extra["Epi"] += 2

    if result["Ema"] >= 6:
        result_extra["Sch"] += 1
        result_extra["Hys"] += 1

    if result["d"] >= 5:
        result_extra["Sch"] += 1

    if result["O"] >= 6:
        result_extra["S"] += 1

    if result["Sex"] == 'M' and result["M"] < result["F"]:
        result_extra["S"] += 1
        result_extra["Sch"] += 1
        result_extra["Hys"] += 1

    if result["Alc"] <= -6:
        result_extra["S"] += 1
    elif result["Alc"] >= 6:
        result_extra["Hys"] += 1

    if result["Mist"] == 1:
        result_extra["Epi"] += 1

    if result["Mist"] >= 2:
        result_extra["Epi"] += 2

    for key in result_extra.keys():
        result[key] += result_extra[key]

def get_sex(line):
    """Checks if the gender is either F or M. If it is not, raises UndefinedGenderError exception"""

    if line == ["M"] or line == ["F"]:
        result["Sex"] = line.pop(0)
    else:
        raise UndefinedGenderError()

try:
    with open(os.path.join(directory_path, answ_file_name)) as answ:
        for i in range(1, 51):
            process_res(*duplicates_checker(*length_checker(answ.readline().split(), i)))
        get_sex(answ.readline().split())

    extra_points()
except DuplicationError as de:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["There are duplicate answer choices in the line", str(de.n)]))
except LengthError as le:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["There is an abnormal quantity of answer choices in the line", str(le.n)]))
except ZeroNotAloneError as ze:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["Zero answer choice is not the only choice in the line", str(ze.n)]))
except OutOfBoundsError as oe:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["The answer choice ", str(oe.answ), " in line", str(oe.n), " is out of the specified bounds. ", "[{!r}, {!r}]".format(answer_choices_left_limit, answer_choices_right_limit[oe.n - 1])]))
except MistypeError as me:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["The answer choice ", str(me.answ), " in line", str(me.n), " is not a possible answer choice"]))
except UndefinedGenderError as uge:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write(' '.join(["The given gender is neither M nor F"]))
else:
    with open(os.path.join(directory_path, res_file_name), 'w') as res:
        res.write('\n'.join("{!s}: {!r}{!s}".format(key, val, " (" + str(result_extra[key]) +")" if key in result_extra.keys() and result_extra[key] != 0 else "") for key, val in result.items()))
